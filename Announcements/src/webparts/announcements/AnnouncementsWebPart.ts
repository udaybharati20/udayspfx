import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './AnnouncementsWebPart.module.scss';
import * as strings from 'AnnouncementsWebPartStrings';
const tile :any = require('./assets/ATile.png');
const squareImg :any = require('./assets/AImage.png');
export interface IAnnouncementsWebPartProps {
  description: string;
  AnnouncementTitle1:string;
  AnnouncementDescription1:string;
  AnnouncementDate1:string;
  Announcement1Link:string;
  AnnouncementTitle2:string;
  AnnouncementDescription2:string;
  AnnouncementDate2:string;
  Announcement2Link:string;
  AnnouncementTitle3:string;
  AnnouncementDescription3:string;
  AnnouncementDate3:string;
  Announcement3Link:string;
  Box1Title:string;
  Box1Description:string;
  Box1Link:string;
  Box2Title:string;
  Box2Description:string;
  Box2Link:string;
}

export default class AnnouncementsWebPart extends BaseClientSideWebPart<IAnnouncementsWebPartProps> {

  // public render(): void {
  // let AnnouncementTitle1:string=this.properties.AnnouncementTitle1==null?"":this.properties.AnnouncementTitle1.length>58?this.properties.AnnouncementTitle1.substr(0,58):this.properties.AnnouncementTitle1;
  // let AnnouncementDescription1:string=this.properties.AnnouncementDescription1==null?"":this.properties.AnnouncementDescription1.length>58?this.properties.AnnouncementDescription1.substr(0,58):this.properties.AnnouncementDescription1;
  // let Box1Title:string=this.properties.Box1Title==null?"":this.properties.Box1Title.length>58?this.properties.Box1Title.substr(0,58):this.properties.Box1Title;
  // let Box1Description:string=this.properties.Box1Description==null?"":this.properties.Box1Description.length>58?this.properties.Box1Description.substr(0,58):this.properties.Box1Description;
  // let AnnouncementTitle2:string=this.properties.AnnouncementTitle2==null?"":this.properties.AnnouncementTitle2.length>58?this.properties.AnnouncementTitle2.substr(0,58):this.properties.AnnouncementTitle2;
  // let AnnouncementDescription2:string=this.properties.AnnouncementDescription2==null?"":this.properties.AnnouncementDescription2.length>58?this.properties.AnnouncementDescription2.substr(0,58):this.properties.AnnouncementDescription2;
  // let Box2Title:string=this.properties.Box2Title==null?"":this.properties.Box2Title.length>58?this.properties.Box2Title.substr(0,58):this.properties.Box2Title;
  // let Box2Description:string=this.properties.Box2Description==null?"":this.properties.Box2Description.length>58?this.properties.Box2Description.substr(0,58):this.properties.Box2Description;
  // let AnnouncementTitle3:string=this.properties.AnnouncementTitle3==null?"":this.properties.AnnouncementTitle3.length>58?this.properties.AnnouncementTitle3.substr(0,58):this.properties.AnnouncementTitle3;
  // let AnnouncementDescription3:string=this.properties.AnnouncementDescription3==null?"":this.properties.AnnouncementDescription3.length>58?this.properties.AnnouncementDescription3.substr(0,58):this.properties.AnnouncementDescription3;
  // this.domElement.innerHTML = `
  //     <div class="${ styles.announcements }">
  //     <div>
  //       <div class="${styles.annTiles}">
  //         <div>
  //           <a href=${this.properties.Box1Link} class="">
  //           <img src="${tile}" alt="Sundar" class="">
  //               <h4>${Box1Title}</h4>
  //               <p style="margin-top:2px;margin-bottom:20px;">${Box1Description}</p>
  //               </a>
  //         </div>
  //       </div>
  //       <div class="${styles.square}">
  //         <div>
  //           <a href=${this.properties.Announcement1Link} class="">
  //               <p>Announcements</p>
  //               <h4>${AnnouncementTitle1}</h4>
  //               <p style="margin-top:5px;margin-bottom:15px;">${AnnouncementDescription1}</p>
  //               <span>${this.properties.AnnouncementDate1}</span>
  //               </a>
  //         </div>
  //       </div>
  //     </div>
  //     <div>
  //       <div class="${styles.square2}">
  //       <div>
  //         <a href=${this.properties.Announcement2Link} class="">
  //             <p>Announcements</p>
  //             <h4>${AnnouncementTitle2}</h4>
  //             <p style="margin-top:-2px;margin-bottom:0px;">${AnnouncementDescription2}</p>
  //             <span>${this.properties.AnnouncementDate2}</span>
  //             </a>
  //       </div>
  //       </div>
  //       <div class="${styles.square3}">
  //       <div>
  //       <img src="${squareImg}" alt="Sundar" class="">
  //       </div>
  //       </div>
  //     </div>
  //     <div>
  //     <div class="${styles.annTiles2}">
  //       <div>
  //         <a href=${this.properties.Box2Link} class="">
  //         <img src="${tile}" alt="Sundar" class="">
  //             <h4>${Box2Title}</h4>
  //             <p style="margin-top:2px;margin-bottom:20px;">${Box2Description}</p>
  //             </a>
  //       </div>
  //     </div>
  //     <div class="${styles.annTiles3}">
  //       <div>
  //         <a href=${this.properties.Announcement3Link} class="">
  //             <p>Announcements</p>
  //             <h4>${AnnouncementTitle3}</h4>
  //             <p style="margin-top:5px;margin-bottom:15px;">${AnnouncementDescription3}</p>
  //             <span>${this.properties.AnnouncementDate3}</span>
  //             </a>
  //       </div>
  //     </div>
  //   </div>
  //     </div>`;
  // }
  public render(): void {
    let AnnouncementTitle1:string=this.properties.AnnouncementTitle1==null?"":this.properties.AnnouncementTitle1.length>58?this.properties.AnnouncementTitle1.substr(0,58):this.properties.AnnouncementTitle1;
    let AnnouncementDescription1:string=this.properties.AnnouncementDescription1==null?"":this.properties.AnnouncementDescription1.length>58?this.properties.AnnouncementDescription1.substr(0,58):this.properties.AnnouncementDescription1;
    let Box1Title:string=this.properties.Box1Title==null?"":this.properties.Box1Title.length>58?this.properties.Box1Title.substr(0,58):this.properties.Box1Title;
    let Box1Description:string=this.properties.Box1Description==null?"":this.properties.Box1Description.length>58?this.properties.Box1Description.substr(0,58):this.properties.Box1Description;
    let AnnouncementTitle2:string=this.properties.AnnouncementTitle2==null?"":this.properties.AnnouncementTitle2.length>58?this.properties.AnnouncementTitle2.substr(0,58):this.properties.AnnouncementTitle2;
    let AnnouncementDescription2:string=this.properties.AnnouncementDescription2==null?"":this.properties.AnnouncementDescription2.length>58?this.properties.AnnouncementDescription2.substr(0,58):this.properties.AnnouncementDescription2;
    let Box2Title:string=this.properties.Box2Title==null?"":this.properties.Box2Title.length>58?this.properties.Box2Title.substr(0,58):this.properties.Box2Title;
    let Box2Description:string=this.properties.Box2Description==null?"":this.properties.Box2Description.length>58?this.properties.Box2Description.substr(0,58):this.properties.Box2Description;
    let AnnouncementTitle3:string=this.properties.AnnouncementTitle3==null?"":this.properties.AnnouncementTitle3.length>58?this.properties.AnnouncementTitle3.substr(0,58):this.properties.AnnouncementTitle3;
    let AnnouncementDescription3:string=this.properties.AnnouncementDescription3==null?"":this.properties.AnnouncementDescription3.length>58?this.properties.AnnouncementDescription3.substr(0,58):this.properties.AnnouncementDescription3;
    this.domElement.innerHTML = `
        <div class="${ styles.announcements }">
        <div>
          <div class="${styles.annTiles}">
            <div>
              <a href=${this.properties.Box1Link} class="">
              <img src="${tile}" alt="Sundar" class="${styles.imgTeam}">
                  <div class="${styles.titleTeam}">${Box1Title}</div>
                  <div class="${styles.descTeam}" style="">${Box1Description}</div>
                  </a>
            </div>
          </div>
          <div class="${styles.square}">
            <div>
              <a href=${this.properties.Announcement1Link} class="">
                  <p>Announcements</p>
                  <div class="${styles.titleAnn}">${AnnouncementTitle1}</div>
                  <div class="${styles.descAnn}">${AnnouncementDescription1}</div>
                  <div class="${styles.dateAnn}">${this.properties.AnnouncementDate1}</div>
                  </a>
            </div>
          </div>
        </div>
        <div>
          <div class="${styles.square2}">
          <div style="padding-right:4px;">
            <a href=${this.properties.Announcement2Link} class="">
                <p>Announcements</p>
                <div  class="${styles.titleAnn}">${AnnouncementTitle2}</div>
                <div class="${styles.descAnn}">${AnnouncementDescription2}</div>
                <div class="${styles.dateAnn}">${this.properties.AnnouncementDate2}</div>
                </a>
          </div>
          </div>
          <div class="${styles.square3}">
          <div>
          <img src="${squareImg}" alt="Sundar" class="">
          </div>
          </div>
        </div>
        <div style="clear:left;" >
        <div class="${styles.annTiles2}">
          <div>
            <a href=${this.properties.Box2Link} class="">
            <img src="${tile}" alt="Sundar" class="${styles.imgTeam}">
                <div class="${styles.titleTeam}">${Box2Title}</div>
                <div  class="${styles.descTeam}">${Box2Description}</div>
                </a>
          </div>
        </div>
        <div class="${styles.annTiles3}">
          <div>
            <a href=${this.properties.Announcement3Link} class="">
                <p>Announcements</p>
                <div class="${styles.titleAnn}">${AnnouncementTitle3}</div>
                <div  class="${styles.descAnn}">${AnnouncementDescription3}</div>
                <div class="${styles.dateAnn}">${this.properties.AnnouncementDate3}</div>
                </a>
          </div>
        </div>
      </div>
        </div>`;
    }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),
                PropertyPaneTextField('Box1Title', {
                  label: strings.Box1Title
                }),
                PropertyPaneTextField('Box1Description', {
                  label: strings.Box1Description
                }),
                PropertyPaneTextField('Box1Link', {
                  label: strings.Box1Link
                }),
                PropertyPaneTextField('AnnouncementTitle1', {
                  label: strings.AnnouncementTitle1
                }),
                PropertyPaneTextField('AnnouncementDescription1', {
                  label: strings.AnnouncementDescription1
                }),
                PropertyPaneTextField('AnnouncementDate1', {
                  label: strings.AnnouncementDate1
                }),
                PropertyPaneTextField('Announcement1Link', {
                  label: strings.Announcement1Link
                }),
                PropertyPaneTextField('AnnouncementTitle2', {
                  label: strings.AnnouncementTitle2
                }),
                PropertyPaneTextField('AnnouncementDescription2', {
                  label: strings.AnnouncementDescription2
                }),
                PropertyPaneTextField('AnnouncementDate2', {
                  label: strings.AnnouncementDate2
                }),
                PropertyPaneTextField('Announcement2Link', {
                  label: strings.Announcement2Link
                }),
                PropertyPaneTextField('Box2Title', {
                  label: strings.Box2Title
                }),
                PropertyPaneTextField('Box2Description', {
                  label: strings.Box2Description
                }),
                PropertyPaneTextField('Box2Link', {
                  label: strings.Box2Link
                }),
                PropertyPaneTextField('AnnouncementTitle3', {
                  label: strings.AnnouncementTitle3
                }),
                PropertyPaneTextField('AnnouncementDescription3', {
                  label: strings.AnnouncementDescription3
                }),
                PropertyPaneTextField('AnnouncementDate3', {
                  label: strings.AnnouncementDate3
                }),
                PropertyPaneTextField('Announcement3Link', {
                  label: strings.Announcement3Link
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
