define([], function() {
  return {
    "PropertyPaneDescription": "Description",
    "BasicGroupName": "Group Name",
    "DescriptionFieldLabel": "Description Field",
    "AnnouncementTitle1":"Announcement 1 Title",
    "AnnouncementDescription1":"Announcement 1 Description",
    "AnnouncementDate1":"Announcement 1 Date",
    "Announcement1Link":"Announcement 1 Link",
    "AnnouncementTitle2":"Announcement 2 Title",
    "AnnouncementDescription2":"Announcement 2 Description",
    "AnnouncementDate2":"Announcement 2 Date",
    "Announcement2Link":"Announcement 2 Link",
    "Box1Title":"Box 1 Title",
    "Box1Description":"Box 1 Description",
    "Box1Link":"Box 1 Link",
    "Box2Title":"Box 2 Title",
    "Box2Description":"Box 2 Description",
    "Box2Link":"Box 2 Link",
    "AnnouncementTitle3":"Announcement 3 Title",
    "AnnouncementDescription3":"Announcement 3 Description",
    "AnnouncementDate3":"Announcement 3 Date",
    "Announcement3Link":"Announcement 3 Link"
  }
});