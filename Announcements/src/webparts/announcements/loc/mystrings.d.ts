declare interface IAnnouncementsWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  AnnouncementTitle1:string;
  AnnouncementDescription1:string;
  AnnouncementDate1:string
  Announcement1Link:string;
  AnnouncementTitle2:string;
  AnnouncementDescription2:string;
  AnnouncementDate2:string;
  Announcement2Link:string;
  Box1Title:string;
  Box1Description:string;
  Box1Link:string;
  Box2Title:string;
  Box2Description:string;
  Box2Link:string;
  AnnouncementTitle3:string;
  AnnouncementDescription3:string;
  AnnouncementDate3:string;
  Announcement3Link:string;
}

declare module 'AnnouncementsWebPartStrings' {
  const strings: IAnnouncementsWebPartStrings;
  export = strings;
}
