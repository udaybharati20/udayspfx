import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './ArchiveListWebPart.module.scss';
import * as strings from 'ArchiveListWebPartStrings';
import {
  SPHttpClient,
  SPHttpClientResponse
} from '@microsoft/sp-http';
export interface IArchiveListWebPartProps {
  description: string;
}
export interface ISPLists {
  value: ISPList[];
}
export interface ISPList {
  Name: string;
  ServerRelativeUrl: object;
}
export default class ArchiveListWebPart extends BaseClientSideWebPart<IArchiveListWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.archiveList }">
        <div class="${ styles.container }">
          <div class="${ styles.row }">
            <h3><a href="">Archives</a></h3>
            <div id="spListContainer" class="${styles.QuickLinks}">

            </div>
          </div>
        </div>
    </div>`;
  this._renderListAsync();
  }
  private _renderListAsync(): void{
    this.getListData()
        .then((response) => {
          this._renderList(response.value);
        });
  }
  private getListData():Promise<ISPLists>
  {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl+"/_api/web/lists/getByTitle('Site%20Pages')/RootFolder/folders",SPHttpClient.configurations.v1)
    .then((response:SPHttpClientResponse)=>{
      return response.json();
    });
  }
  private _renderList(items: ISPList[]): void 
  {
   let html: string = '<ul>';
   console.log("archives "+ items);
   items.forEach((item: ISPList) => {
    let linkURL=item.ServerRelativeUrl;
    if(item.Name != "Forms"){
      html += `
      <li><a href="${linkURL}">${item.Name}</a></li>
          `;
      }
   });
 html += '</ul>';
   const listContainer: Element = this.domElement.querySelector('#spListContainer');
   listContainer.innerHTML = html;
 }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
