declare interface IArchiveListWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'ArchiveListWebPartStrings' {
  const strings: IArchiveListWebPartStrings;
  export = strings;
}
