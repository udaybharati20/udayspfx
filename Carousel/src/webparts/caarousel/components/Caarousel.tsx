import * as React from 'react';
import styles from './Caarousel.module.scss';

import { ICaarouselProps } from './ICaarouselProps';
import { escape } from '@microsoft/sp-lodash-subset';
require('./image-gallery.css');
import ImageGallery from 'react-image-gallery';
import * as jquery from 'jquery';
import { split } from 'lodash';

export interface ISPLists {
  value: ISPList[];
}
export interface ISPList {
Title: string;
Link: string;
VideoUrl :string;
FileRef:string;
Created:string;
}
const PREFIX_URL = 'https://raw.githubusercontent.com/xiaolin/react-image-gallery/master/static/';
export default class Caarousel extends React.Component<ICaarouselProps, any> {
  images: ({ original: string; embedUrl: string; description: string; renderItem: any; originalClass?: undefined; thumbnailClass?: undefined; created: string; } | { original: string; originalClass: string; thumbnailClass: string; description: string; embedUrl?: undefined; renderItem?: undefined;created: string; })[];
  private _imageGallery: any;
  constructor() {
    super(null);
    this.state = {
      showIndex: false,
      showBullets: true,
      infinite: true,
      showThumbnails: false,
      showFullscreenButton: false,
      showGalleryFullscreenButton: false,
      showPlayButton: false,
      showGalleryPlayButton: true,
      showNav: false,
      isRTL: false,
      slideDuration: 450,
      slideInterval: 9000,
      slideOnThumbnailOver: false,
      thumbnailPosition: 'bottom',
      showVideo: {},
      useWindowKeyDown: true,
      autoPlay:true,
      Items: [ {    
        "FileRef": "",    
        "Title": "",    
        "Link":"",    
        "VideoUrl":""  
      }]
    };
    this.images = [
      {
        original: 'https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Carousel/bannerSlider.png',
        originalClass: 'featured-slide',
        thumbnailClass: 'featured-thumb',
        description: '100 Years of Powering Agriculture',
        created:'Thursday, Sep 06 2021',
        renderItem: this.myRenderItem.bind(this)
      },
      {
        original: 'https://www.cummins.com/sites/default/files/images/newsroom_article/100_fb%20cover%20photos_final-03.jpg',
        originalClass: 'featured-slide',
        thumbnailClass: 'featured-thumb',
        description: 'Cummins Common Services',
        created:'Tuesday, Aug 01 2021',
        renderItem: this.myRenderItem.bind(this)
      },
     
      {
        original: 'https://www.cummins.com/sites/default/files/styles/newsroom_hero_image/public/images/newsroom_article/blog-article-default-hero-cummins-logo-building-exterior_11.jpg',
        embedUrl: 'https://www.youtube.com/embed/brn9uUABpCA?autoplay=1&showinfo=0',
        description: 'Welcoming Environment at Cummins',
        created:'Wednesday, Sep 18 2021',
        renderItem: this._renderVideo.bind(this)
      },  
      
      {
        original: 'https://di-uploads-pod19.dealerinspire.com/internationalusedtruckcenters/uploads/2018/12/IUTC-Cummins-Banner-1400x500-white_3.6MB_April22.jpg',
        originalClass: 'featured-slide',
        thumbnailClass: 'featured-thumb',
        description: 'Cummins Engine',
        created:'Monday, Jul 13 2021',
        renderItem: this.myRenderItem.bind(this)
      },
      {
        original: 'https://ddjkm7nmu27lx.cloudfront.net/667853/fileUpload/667853_1573598064350_WebBannerOptions5.png',
        originalClass: 'featured-slide',
        thumbnailClass: 'featured-thumb',
        description: 'Cummins Vision',
        created:'Friday, Aug 27 2021',
        renderItem: this.myRenderItem.bind(this)
      }
    ];
  }
  public componentDidMount(){    
    var reactHandler = this;    
    this.getListData();
  } 
  private getListData()
  {
    console.log("componentDidMount");
    let imgArray:any[]=[];
    jquery.ajax({    
        url: `${this.props.webURL}/_api/web/lists/getbytitle('PictureCarousel')/items?$select=FileRef,Title,Link,VideoUrl,SortOrder,Active,Created&$filter=Active eq 1&$top=5&$orderby= SortOrder asc`,    
        type: "GET",    
        headers:{'Accept': 'application/json; odata=verbose;'},  
        async:false,  
        success: function(resultData) {    
            
          console.log("componentDidMount success");
          console.log("state"+resultData);
          console.log(resultData.d.results);
        
          resultData.d.results.forEach((item: ISPList) => {
            imgArray.push({
              original : item.FileRef,
              originalClass: 'featured-slide',
              thumbnailClass: 'featured-thumb',
              description: item.Title,
              created:item.Created,
              videoUrl: item.VideoUrl,
              Link:item.Link== null?"":item.Link["Url"]
            });
            //imgArray.push({item});
          });
          // console.log("renderList"+imgArray);
          // this.images = imgArray;
          // this.setState({ Items: imgArray });
          // console.log("after setting items");
          // reactHandler.setState({    
          //   Items: resultData.d.results    
          // }); 
        },    
        error : function(jqXHR, textStatus, errorThrown) {   
          console.log("componentDidMount error"); 
        }    
    });  
    console.log("renderList");
    console.log(imgArray);
    let itemArray:any[]=[];
    imgArray.forEach((item:any)=>{
      if(item.videoUrl === null){
      itemArray.push({
              original : item.original,
              originalClass: 'featured-slide',
              thumbnailClass: 'featured-thumb',
              description: item.description,
              created:this.dateFormatter(item.created),
              Link:item.Link,
              renderItem: this.myRenderItem.bind(this)
            });
          }
          else{
            itemArray.push({
              original : item.original,
              embedUrl: item.videoUrl,
              description: item.description,
              created:this.dateFormatter(item.created),
              Link:item.Link,
              renderItem: this._renderVideo.bind(this)
            });
          }
    });
    this.images = itemArray;
    this.setState({ Items: itemArray });
  }

  private myRenderItem (item) {
    return <div>
          <img className="image-gallery-image" src={item.original} title="Image" />
          <span style={{left: '0px !important;'}} className="image-gallery-description"><a className="" href={item.Link}> {item.description}</a></span>
          <span style={{left: '0px !important;'}} className="image-gallery-Author"> {item.created}</span>
      </div>;
  }
  private _renderVideo(item) {
    return (
      <div>
        {
          this.state.showVideo[item.embedUrl] ?
            <div className='video-wrapper'>
                <a
                  className='close-video'
                  onClick={this._toggleShowVideo.bind(this, item.embedUrl)}
                >
                </a>
                <iframe
                  width='560'
                  height='315'
                  src={item.embedUrl}
                  frameBorder='0'
                  allowFullScreen
                >
                </iframe>
            </div>
          :
            <a onClick={this._toggleShowVideo.bind(this, item.embedUrl)}>
              <div className="play-button"></div>
              <img className='image-gallery-image' src={item.original} />
              {
                item.description &&
                  <span
                    className='image-gallery-description'
                    style={{left: '0px !important;'}}
                  >
                    {item.description}
                  </span>
                 
              }
              <span
                    className='image-gallery-Author'
                    style={{left: '0px !important;'}}
                  >
                    {item.created}
                  </span>
            </a>
        }
      </div>
    );
  }
  private _toggleShowVideo(url) {
    this.state.showVideo[url] = !Boolean(this.state.showVideo[url]);
    this.setState({
      showVideo: this.state.showVideo
    });

    if (this.state.showVideo[url]) {
      if (this.state.showPlayButton) {
        this.setState({showGalleryPlayButton: false});
      }

      if (this.state.showFullscreenButton) {
        this.setState({showGalleryFullscreenButton: false});
      }
    }
  }
  private _onImageClick(event) {
    console.debug('clicked on image', event.target, 'at index', this._imageGallery.getCurrentIndex());
  }

  private _onImageLoad(event) {
    console.debug('loaded image', event.target.src);
  }

  private _onSlide(index) {
    this._resetVideo();
    console.debug('slid to index', index);
  }
  private _resetVideo() {
    this.setState({showVideo: {}});

    if (this.state.showPlayButton) {
      this.setState({showGalleryPlayButton: true});
    }

    if (this.state.showFullscreenButton) {
      this.setState({showGalleryFullscreenButton: true});
    }
  }
  private _onPause(index) {
    console.debug('paused on index', index);
  }

  private _onScreenChange(fullScreenElement) {
    console.debug('isFullScreen?', !!fullScreenElement);
  }

  private _onPlay(index) {
    console.debug('playing from index', index);
  }

  private _handleInputChange(state, event) {
    if (event.target.value > 0) {
      this.setState({[state]: event.target.value});
    }
  }

  private _handleCheckboxChange(state, event) {
    this.setState({[state]: event.target.checked});
  }

  private _handleThumbnailPositionChange(event) {
    this.setState({thumbnailPosition: event.target.value});
  }
  public render(): React.ReactElement<ICaarouselProps> {
    return <div className={ styles.caarousel }><ImageGallery 
    ref={i => this._imageGallery = i}
    items={this.images} 
    onClick={this._onImageClick.bind(this)}
          onImageLoad={this._onImageLoad}
          onSlide={this._onSlide.bind(this)}
          onPause={this._onPause.bind(this)}
          onScreenChange={this._onScreenChange.bind(this)}
          onPlay={this._onPlay.bind(this)}
          infinite={this.state.infinite}
          showBullets={this.state.showBullets}
          showFullscreenButton={false}
          showPlayButton={false}
          showThumbnails={false}
          showIndex={this.state.showIndex}
          showNav={true}
          autoPlay={true}
          isRTL={this.state.isRTL}
          thumbnailPosition={this.state.thumbnailPosition}
          slideDuration={parseInt(this.state.slideDuration)}
          slideInterval={parseInt(this.state.slideInterval)}
          slideOnThumbnailOver={this.state.slideOnThumbnailOver}
          additionalClass="app-image-gallery"
          useWindowKeyDown={this.state.useWindowKeyDown}
    /></div>;
    // return (
    //   <div className={ styles.caarousel }>
    //     <div className={ styles.container }>
    //       <div className={ styles.row }>
    //         <div className={ styles.column }>
    //           <span className={ styles.title }>Welcome to SharePoint!</span>
    //           <p className={ styles.subTitle }>Customize SharePoint experiences using Web Parts.</p>
    //           <p className={ styles.description }>{escape(this.props.description)}</p>
    //           <a href="https://aka.ms/spfx" className={ styles.button }>
    //             <span className={ styles.label }>Learn more</span>
    //           </a>
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    // );
  }
 private dateFormatter(datetoFormat:string):string
 {
  let date:string = "";
  var options:any = { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric' };
  var objDate  = new Date(datetoFormat); 
  date = objDate.toLocaleDateString("en-US", options).toString();
  return date;
 }
}
