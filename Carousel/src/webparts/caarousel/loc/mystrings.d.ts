declare interface ICaarouselWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CaarouselWebPartStrings' {
  const strings: ICaarouselWebPartStrings;
  export = strings;
}
