import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  PropertyPaneLink
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './CategoryListWebPart.module.scss';
import * as strings from 'CategoryListWebPartStrings';
import {
  SPHttpClient,
  SPHttpClientResponse
} from '@microsoft/sp-http';
export interface ICategoryListWebPartProps {
  description: string;
  ListURL: string;
}
export interface ISPLists {
  value: ISPList[];
}
export interface ISPList {
Title: string;
}
export default class CategoryListWebPart extends BaseClientSideWebPart<ICategoryListWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.categoryList }">
        <div class="${ styles.container }">
          <div class="${ styles.row }">
          <h3><a href="#">Category</a></h3>
          <div id="spListContainer" class="${styles.QuickLinksCSS}">

          </div>
          </div>
        </div>
      </div>`;
      this._renderListAsync();
  }
  // public render(): void {
  //   this.domElement.innerHTML = `
  //     <div class="${ styles.categoryList }">
  //       <div class="${ styles.container }">
  //         <div class="${ styles.row }">
  //           <div class="${ styles.column }">
  //             <span class="${ styles.title }">Welcome to SharePoint!</span>
  //             <p class="${ styles.subTitle }">Customize SharePoint experiences using Web Parts.</p>
  //             <p class="${ styles.description }">${escape(this.properties.description)}</p>
  //             <a href="https://aka.ms/spfx" class="${ styles.button }">
  //               <span class="${ styles.label }">Learn more</span>
  //             </a>
  //           </div>
  //         </div>
  //       </div>
  //     </div>`;
  // }
  private _renderListAsync(): void{
    this.getListData()
        .then((response) => {
          this._renderList(response.value[0].Choices);
        });
  }
  private _renderList(items: any[]): void 
  {
  console.log(items);
   let html: string = '<ul>';
   items.forEach((item: ISPList) => {
    let linkvalue= item;
    let linkURL="https://cummins365.sharepoint.com/sites/CS45455_dev/SitePages/ABC.aspx?Category="+linkvalue;
     html += `
     <li><a target="_blank" href="${linkURL}">${item}</a></li>
         `;
   });
 html += '</ul>';
   const listContainer: Element = this.domElement.querySelector('#spListContainer');
   listContainer.innerHTML = html;
 }
  private getListData():Promise<any>
  {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl+"/_api/web/lists/getbytitle('Site%20Pages')/fields?$filter=EntityPropertyName%20eq%20%27Category%27",SPHttpClient.configurations.v1)
    .then((response:SPHttpClientResponse)=>{
      return response.json();
    });
  }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),
                PropertyPaneLink('ListURL',{
                  text:'Click here to configure categories',href:'https://cummins365.sharepoint.com/sites/CS45455_dev/_layouts/15/FldEdit.aspx?List=%7B39D50A69%2D5220%2D4A67%2DB562%2D9463574FF6C6%7D&Field=Category',target:'_blank'
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
