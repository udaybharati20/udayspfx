declare interface ICategoryListWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CategoryListWebPartStrings' {
  const strings: ICategoryListWebPartStrings;
  export = strings;
}
