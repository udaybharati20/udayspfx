import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './MostReadArticlesWebPart.module.scss';
import * as strings from 'MostReadArticlesWebPartStrings';
import { SPHttpClient, SPHttpClientResponse } from '@microsoft/sp-http';
export interface IMostReadArticlesWebPartProps {
  description: string;
}
export interface ISPLists {
  value: ISPList[];
}
export interface ISPList  
{  
    Title: string;  
    path: string;  
    ViewsLifeTime: string;  
}  
export default class MostReadArticlesWebPart extends BaseClientSideWebPart<IMostReadArticlesWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
      <div  class="${ styles.mostReadArticles }">
      <div class="${ styles.container }">
      <div class="${ styles.row }">
      <button><a href="https://cummins365.sharepoint.com/sites/CS45455_dev/_layouts/15/CreateSitePage.aspx">CREATE NEW ARTICLE<a></button>
      <h3><a href="">Most Read Articles</a></h3>
      <div id="spListContainer" class="${styles.QuickLinks}">

      </div>
      </div>
    </div>
      </div>`;
      this._renderListAsync();
  }
  private _renderListAsync(): void{
    this.getSearchResults()
        .then((result : any[]) => {
          console.log("inside then"); 
            this._renderList(result);
        });
  }
  private _isNull(value: any): boolean {  
    return value === null || typeof value === "undefined";  
}  
private _setSearchResults(crntResults: any[], fields: string): any[] {  
  console.log("_setSearchResults");
  const temp: any[] = [];  

  if (crntResults.length > 0) {  
      const flds: string[] = fields.toLowerCase().split(',');  

      crntResults.forEach((result) => {  
          // Create a temp value  
          var val: Object = {};  

          result.Cells.forEach((cell: any) => {  
              if (flds.indexOf(cell.Key.toLowerCase()) !== -1) {  
                  // Add key and value to temp value  
                  val[cell.Key] = cell.Value;  
              }  
          });  

          // Push this to the temp array  
          temp.push(val);  
          console.log("val"+val);
      });  
  }  

  return temp;  
}

public getSearchResults(): Promise<ISPList[]> {  
          
  return new Promise<ISPList[]>((resolve, reject) => {  
      // Do an Ajax call to receive the search results  
      this.getListData().then((res: any) => {  
          let searchResp: ISPList[] = [];  

          // Check if there was an error  
          if (typeof res["odata.error"] !== "undefined") {  
              if (typeof res["odata.error"]["message"] !== "undefined") {  
                  Promise.reject(res["odata.error"]["message"].value);  
                  return;  
              }  
          }  

          if (!this._isNull(res)) {  
              const fields: string = "Title,Path,Description";  

              // Retrieve all the table rows  
              if (typeof res.PrimaryQueryResult.RelevantResults.Table !== 'undefined') {  
                  if (typeof res.PrimaryQueryResult.RelevantResults.Table.Rows !== 'undefined') {  
                      searchResp = this._setSearchResults(res.PrimaryQueryResult.RelevantResults.Table.Rows, fields);  
                  }  
              }  
          }  

          // Return the retrieved result set  
          resolve(searchResp);  
      });  
  });  
} 
  private getListData():Promise<ISPLists>
  {
    let query = this.context.pageContext.web.absoluteUrl+"/_api/search/query?querytext='ContentType:Site Page+path:https://cummins365.sharepoint.com/sites/CS45455_dev/'&selectproperties='Title,path,ViewsLifeTime,ViewsLifeTimeUniqueUsers'&sortlist='ViewsLifeTime:descending'&rowlimit=10";
    console.log("query"+query);
    return this.context.spHttpClient.get(query,SPHttpClient.configurations.v1, {  
      headers: {  
          'odata-version': '3.0'  
      }  
  }).then((response:SPHttpClientResponse)=>{
      console.log("B"+response.status);
      return response.json();
    }).catch(error => {  
      return Promise.reject(JSON.stringify(error));  
  }); 
  }
  private _renderList(items: any[]): void 
   {
    let html: string = '<ul>';
    items.forEach((item: ISPList) => {
      let linkTitle = item["Title"];
      let linkURL=item["path"];
      if(linkTitle != "COMMON SERVICES" && linkTitle != "ABC" && linkTitle != "Common Services Organization" && linkTitle != "Common Services" && linkTitle !="Thought Leadership"){
       html += `
       <li><a href="${linkURL}">${linkTitle}</a></li>
           `;
      }
    });
    html += '</ul>';
    const listContainer: Element = this.domElement.querySelector('#spListContainer');
    listContainer.innerHTML = html;
    // let html: string = '<div>ABC</div>';
    // const listContainer: Element = this.domElement.querySelector('#spListContainer');
    // listContainer.innerHTML = html;
    console.log(items.length);
    console.log(items);
  }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
