declare interface IMostReadArticlesWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'MostReadArticlesWebPartStrings' {
  const strings: IMostReadArticlesWebPartStrings;
  export = strings;
}
