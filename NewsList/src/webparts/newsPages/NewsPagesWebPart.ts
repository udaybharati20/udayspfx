import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './NewsPagesWebPart.module.scss';
require('./Newlist.css');
import * as strings from 'NewsPagesWebPartStrings';
import {
  SPHttpClient,
  SPHttpClientResponse
} from '@microsoft/sp-http';
import { UrlQueryParameterCollection } from '@microsoft/sp-core-library';
export interface INewsPagesWebPartProps {
  description: string;
}
export interface ISPLists {
  value: ISPList[];
}
export interface ISPList {
Title: string;
Description: string;
FileRef:string;
AuthorID:number;
Category:string;
Created:string;
BannerImageUrl:object;
likeCount:number;
itemID: number;
}
export const getDay = (date): string => {
  var date1 = new Date(date);
  var day = date1.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  return day;
};
export const getMonth = (date): string => {
  var date1 = new Date(date);
  var month = date1.getMonth();
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"
];
  var mName = monthNames[month];
  return mName;
};
export const getTime = (date): string => {
  var date1 = new Date(date).toLocaleString('en-US', { timeZone: 'America/New_York' });
  var time = date1.split(' ')[1];
  return time;
};
export const FormatDate = (date): string => {
  var date1 = new Date(date);
  var year = date1.getFullYear();
  var month = getMonth(date);
  var day = date1.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  return month + ' ' + day + ', ' + year;
};
export default class NewsPagesWebPart extends BaseClientSideWebPart<INewsPagesWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
      <div id="spListContainer" class="${ styles.newsPages }">

      </div>`;
      this._renderListAsync();
  }
  private _renderListAsync(): void{
    this.getListData()
        .then((response) => {
          this._renderList(response.value);
        });
  }
  private renderLikeCount(item: any):number
  {
    console.log ("1"+item);
    let likesCont= item["likeCount"];
    console.log(likesCont);
    return likesCont;
  }
  private getListData():Promise<ISPLists>
  {
    var queryParms = new UrlQueryParameterCollection(window.location.href);
    var categoryParm = queryParms.getValue("Category");
    let queryURL = "/_api/web/lists/getbytitle('Site%20Pages')/items?$select=Title,Id,FileRef,Created,CanvasContent1,Category,BannerImageUrl,Author/EMail,Author/Title,likedByInformation,Description&$expand=likedByInformation,Author&$filter=Category ne null&$top=10";
    if(categoryParm != null)
    {
      queryURL = "/_api/web/lists/getbytitle('Site%20Pages')/items?$select=Title,Id,FileRef,Created,CanvasContent1,Category,BannerImageUrl,Author/EMail,Author/Title,likedByInformation,Description&$expand=likedByInformation,Author&$filter=Category eq '"+categoryParm+"'&$top=10";
    }
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl+queryURL,SPHttpClient.configurations.v1)
    .then((response:SPHttpClientResponse)=>{
      return response.json();
    });
  }
  private getLikesData(itemID : number):Promise<ISPList>
  {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl+"/_api/web/lists/getbytitle('Site%20Pages')/GetItemById(8)/likedByInformation?$expand=likedby",SPHttpClient.configurations.v1)
    .then((response:SPHttpClientResponse)=>{
      return response.json();
    });
  }
 

  private _renderList(items: ISPList[]): void 
   {
    let html: string = '';
    items.forEach((item: ISPList) => {
      let articleURL = item.FileRef;
      let bImageTemp = item.BannerImageUrl["Url"];
      let bImage= bImageTemp.replace('amp;','');
      let author = item["Author"];
      let createdBy = author.Title;
      let AuthorEmail = author.EMail;
      let profilePicURL= "https://cummins365.sharepoint.com/_layouts/15/userphoto.aspx?size=M&username="+AuthorEmail;
      let desc = item.Description===null?"Please click article title to read more...":item.Description.length>190?item.Description.substr(0,190)+"...":item.Description;
      // let countOfLikes = this.test();
      if(item.Title != null){
      html += `
      <div class="card card--text card--show-more">
      <div class="preview">
      <a class="preview__link ratio1x2" href="${articleURL}"><img src="${bImage}" class="${styles.cardImage}"></img></a>
      </div>
      <div class="card__text">
        <div class="divCategory">${item.Category}</div>   
        <a class="headlink h4--dense" href="${articleURL}" >
          <div>${item.Title}</div>
        </a>
        <div class="cardDesc" style="">${desc}</div>
        <div class="byline">
          <div class="byline__author-group">
            <div class="byline__author">
            <div style="float:left;"><img src="${profilePicURL}" alt="" class="${styles.messagecardImage}"></div>
              <div class="byline__by"> By</div>
              <div class="byline__author-name">${createdBy}</div>
              <div class="byline__author-type">Added on : <b>${FormatDate(item.Created)}</b></div>
            </div>
          </div>
        </div>
      </div>
    </div>
          `;
      }
    });
    const listContainer: Element = this.domElement.querySelector('#spListContainer');
    listContainer.innerHTML = html;
  }
//   private _renderList(items: ISPList[]): void 
//   {
//    let html: string = '';
//    items.forEach((item: ISPList) => {
//      let articleURL = item.FileRef;
//      let bImageTemp = item.BannerImageUrl["Url"];
//      let bImage= bImageTemp.replace('amp;','');
//      let author = item["Author"];
//      let createdBy = author.Title;
//      let AuthorEmail = author.EMail;
//      let profilePicURL= "https://cummins365.sharepoint.com/_layouts/15/userphoto.aspx?size=M&username="+AuthorEmail;
//      let desc = item.Description===null?"Please click article title to read more...":item.Description.length>190?item.Description.substr(0,190)+"...":item.Description;
//      // let countOfLikes = this.test();
//      if(item.Title != null){
//      html += `
//      <div class="card card--text card--show-more">
//      <div class="preview">
//      <a class="preview__link ratio1x2" href="${articleURL}"><img src="${bImage}" class="${styles.cardImage}"></img></a>
//      </div>
//      <div class="card__text">
//      <div class="divCategory">${item.Category}</div>   
//        <a class="headlink h4--dense" href="${articleURL}" >
//          <span>${item.Title}</span>
//        </a>
//        <p style="margin-top: 5px;font-size: 11px;">${desc}</p>
//        <span class="byline">
//          <div class="byline__author-group">
//            <div class="byline__author">
//            <div style="float:left;"><img src="${profilePicURL}" alt="" class="${styles.messagecardImage}"></div>
//              <span class="byline__by"> By</span>
//              <span class="byline__author-name">${createdBy}</span>
//              <span class="byline__author-type">Added on : <b>${FormatDate(item.Created)}</b></span>
//            </div>
//          </div>
//        </span>
//      </div>
//    </div>
//          `;
//      }
//    });
//    const listContainer: Element = this.domElement.querySelector('#spListContainer');
//    listContainer.innerHTML = html;
//  }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }
 
  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
