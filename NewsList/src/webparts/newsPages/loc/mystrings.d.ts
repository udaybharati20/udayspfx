declare interface INewsPagesWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'NewsPagesWebPartStrings' {
  const strings: INewsPagesWebPartStrings;
  export = strings;
}
