import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';

import * as strings from 'PeopleCarouselWebPartStrings';
import PeopleCarousel from './components/PeopleCarousel';
import { IPeopleCarouselProps } from './components/IPeopleCarouselProps';
//import {IPropertyFieldGroupOrPerson, PrincipalType, PropertyFieldPeoplePicker} from "@pnp/spfx-property-controls/lib/PropertyFieldPeoplePicker";
export interface IPeopleCarouselWebPartProps {
  description: string;
  UserWWID:string;
}

export default class PeopleCarouselWebPart extends BaseClientSideWebPart<IPeopleCarouselWebPartProps> {

  public render(): void {
    const element: React.ReactElement<IPeopleCarouselProps> = React.createElement(
      PeopleCarousel,
      {
        description: this.properties.description,
        webURL:this.context.pageContext.web.absoluteUrl,
        UserAccountName:this.properties.UserWWID
      }
    );
    console.log("User:"+ this.properties.UserWWID);
    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }
  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),
                PropertyPaneTextField('UserWWID', {
                  label: "Enter WWID of User"
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
