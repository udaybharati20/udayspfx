import * as React from 'react';
import styles from './PeopleCarousel.module.scss';
import { IPeopleCarouselProps } from './IPeopleCarouselProps';
import { escape } from '@microsoft/sp-lodash-subset';
import Carousel from 'react-multi-carousel';
import * as jquery from 'jquery';
//import "react-multi-carousel/lib/styles.css";
require('./app.css');
export interface ISPList {
  Title: string;
  DisplayName: string;
  PictureUrl :string;
  }
export default class PeopleCarousel extends React.Component<IPeopleCarouselProps, any> {
  directReportsArr: any[];
  directReportsData: ISPList[];
  deptImg:string[];
  constructor() {
    super(null);
    this.state = {    
      items: []  
    }; 
    this.directReportsArr = [];
    this.directReportsData = [];
    this.deptImg = [
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/01.jpg",
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/02.jpg",
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/03.jpg",
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/01.jpg",
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/02.jpg",
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/03.jpg",
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/01.jpg",
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/02.jpg",
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/03.jpg",
      "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/01.jpg",
       "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/02.jpg",
       "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/03.jpg",
       "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/01.jpg",
       "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/02.jpg",
       "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/03.jpg"
    ];
  }
  public componentDidMount(){    
    //console.log("selected user:"+this.props.UserAccountName);
    let i:number= 0;
    let dReportsData:ISPList[]=[]; 
    this.getDirectReportsData();
    this.directReportsArr.forEach((account)=>{
      i++;
      this.getDirectReportDetails(account,this.deptImg[i]);

    //   let empData: any[]=[];
    //   let aName = account.replace('#','%23');
    //   console.log("getDirectReportDetails");
    //   let getProfileURL = `${this.props.webURL}/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName='`+aName+`')?$select=DisplayName,PictureUrl,Title`;
    //   console.log(getProfileURL);
    //   jquery.ajax({    
    //     url: getProfileURL,    
    //     type: "GET",    
    //     headers:{'Accept': 'application/json; odata=verbose;'},  
    //     async:false,  
    //     success: function(resultData) {   
    //       console.log("getDirectReportDetails Success");
    //       console.log(resultData.d.DisplayName);
    //       empData.push({
    //         DisplayName:resultData.d.DisplayName,
    //         PictureUrl : resultData.d.PictureUrl,
    //         Title:resultData.d.Title
    //       });
    //     },    
    //     error : function(jqXHR, textStatus, errorThrown) {   
    //       console.log("componentDidMount error"); 
         
    //     }    
    // });
    // console.log(empData);
    // let emp:ISPList = empData[0];
    // this.directReportsData.push(emp);
    });
    //console.log("dReportsData");
    //console.log(this.directReportsData);
    this.setState({ items: this.directReportsData });
  } 
  private getDirectReportsData()
  {
    let dArray:any[]=[];
    //console.log("getListData");
    let wwid= this.props.UserAccountName;
    let userUrl = `${this.props.webURL}/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName='i:0%23.f|membership|${wwid}@cummins.com')?$select=DirectReports,AccountName`;
    //console.log("userUrl"+userUrl);
    jquery.ajax({    
      url: userUrl,    
      type: "GET",    
      headers:{'Accept': 'application/json; odata=verbose;'},  
      async:false,  
      success: function(resultData) {   
        //console.log(resultData.d.DirectReports.results);
        dArray = resultData.d.DirectReports.results;
      },    
      error : function(jqXHR, textStatus, errorThrown) {   
        console.log("componentDidMount error"); 
      }    
  });
  //console.log(dArray);
  this.directReportsArr = dArray;
  }
  private getDirectReportDetails(accountName:string,deptImg:string)
  {
    let empData: any[]=[];
    let empName:string="";
    let aName = accountName.replace('#','%23');
    //console.log("getDirectReportDetails");
    let getProfileURL = `${this.props.webURL}/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName='`+aName+`')?$select=DisplayName,PictureUrl,Title,EMail`;
    //console.log(getProfileURL);
    jquery.ajax({    
      url: getProfileURL,    
      type: "GET",    
      headers:{'Accept': 'application/json; odata=verbose;'},  
      async:false,  
      success: function(resultData) {   
        //ole.log("getDirectReportDetails Success");
        //console.log(resultData.d.DisplayName);
        empName= resultData.d.DisplayName;
          empData.push({
            DisplayName:resultData.d.DisplayName,
            PictureUrl : resultData.d.PictureUrl==null?"https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/userphoto.jpg": resultData.d.PictureUrl,
            Title:resultData.d.Title,
            Email: "https://cummins365.sharepoint.com/_layouts/15/userphoto.aspx?size=L&username="+resultData.d.Email,
            DeptImage:deptImg
          });
      },    
      error : function(jqXHR, textStatus, errorThrown) {   
        console.log("componentDidMount error"); 
       
      }    
  });
  //console.log(empData);
  let emp:ISPList = empData[0];
  if(empName!= "Sondra J Dutka"){
  this.directReportsData.push(emp);
  }
  }

  // public render(): React.ReactElement<IPeopleCarouselProps> {
  //   const responsive = {
  //     superLargeDesktop: {
  //       // the naming can be any, depends on you.
  //       breakpoint: { max: 4000, min: 3000 },
  //       items: 5
  //     },
  //     desktop: {
  //       breakpoint: { max: 3000, min: 1024 },
  //       items: 5
  //     },
  //     tablet: {
  //       breakpoint: { max: 1024, min: 464 },
  //       items: 3
  //     },
  //     mobile: {
  //       breakpoint: { max: 464, min: 0 },
  //       items: 2
  //     }
  //   };
  //   return (
  //     <div className={ styles.peopleCarousel }>
  //       <div><p className="listHeaderTitle">Common Services Organization</p>
  //       <a className="listHeaderAll" href="https://cummins365.sharepoint.com/sites/CS45455_dev/SitePages/Common_Services_Org.aspx"><button className="listHeaderAllBtn">View All</button></a>
  //       </div>
  //       <div></div>
  //       <Carousel responsive={responsive} itemClass="carousel-item-padding-10-px"> 
  //       <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/Cummins_614eb43fbfb2188b89ca396ea5f4660a.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/agam_c7ceea832fabef1be2f67bb66cfa2e81.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Agam P Singh</h3>
  //           <p className="messagecardQuote">Global Digital Center</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>Automations, Scalable, Transactional Applications,Low Code Solutions</p>
  //             </div>
  //         </div>
  //       </div> 

  //         <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/01.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/02-cara.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Cara C Corwin</h3>
  //           <p className="messagecardQuote">IT Planning</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>EPMO, PPM, BPT, ITOM</p>
  //             </div>
  //         </div>
  //       </div>

  //       <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/03.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/03-jane.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Jane C Richards</h3>
  //           <p className="messagecardQuote">IT and Corporate Strategy</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>IT Strategy, CIO LnD</p>
  //             </div>
  //         </div>
  //       </div>

  //       <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/04.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/04-jessica.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Jessica Yeo</h3>
  //           <p className="messagecardQuote">Enterprise Architecture</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>Enterprise Arch, Security Architecture </p>
  //             </div>
  //         </div>
  //       </div>

  //       <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/05.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/05-saroja.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Saroja Bharath</h3>
  //           <p className="messagecardQuote">Enterprise Integration Services</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>Integrations, EDI, SDLC & Test Services</p>
  //             </div>
  //         </div>
  //       </div> 

  //         <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/01.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/09-Jack.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Jack Tregoning</h3>
  //           <p className="messagecardQuote">Enterprise Data Architect</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>EPMO, PPM, BPT, ITOM</p>
  //             </div>
  //         </div>
  //       </div>

  //       <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/02.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/07-monica.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Monica J Field</h3>
  //           <p className="messagecardQuote">IT Director – Identity and Access Management</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>Identity and Access Management</p>
  //             </div>
  //         </div>
  //       </div>

  //       <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/04.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/08-Kathy.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Kathy M Hohenstreiter</h3>
  //           <p className="messagecardQuote">Common Services - DBA Leader</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>DBA Leader</p>
  //             </div>
  //         </div>
  //       </div>

  //       <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/Cummins_614eb43fbfb2188b89ca396ea5f4660a.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/10-sherwin.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Sherwin Telfer</h3>
  //           <p className="messagecardQuote">Cloud Migration Senior Project Manager</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>Senior Project Manager</p>
  //             </div>
  //         </div>
  //       </div> 

  //         <div className="messagecardWrap">
  //         <div className="messagecard">
  //         <div className="divImageContainer"><img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/03.jpg" alt="director" className="messageHeaderImg"></img></div>
  //           <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/PeopleSlider/06-vanessa.jpg" alt="Sundar" className="messagecardImage"></img>
  //           <h3 className="messagecardName">Vanessa E Cunningham</h3>
  //           <p className="messagecardQuote">IT Director - Employee Experience</p>
  //           <div className="messagecardCompanyInfo">
  //             <p>IT Director - Employee Experience</p>
  //             </div>
  //         </div>
  //       </div>
  //       </Carousel>
  //       </div>
  //   );
  //   // return (
  //   //   <div className={ styles.peopleCarousel }>
  //   //     <div className={ styles.container }>
  //   //       <div className={ styles.row }>
  //   //         <div className={ styles.column }>
  //   //           <span className={ styles.title }>Welcome to SharePoint!</span>
  //   //           <p className={ styles.subTitle }>Customize SharePoint experiences using Web Parts.</p>
  //   //           <p className={ styles.description }>{escape(this.props.description)}</p>
  //   //           <a href="https://aka.ms/spfx" className={ styles.button }>
  //   //             <span className={ styles.label }>Learn more</span>
  //   //           </a>
  //   //         </div>
  //   //       </div>
  //   //     </div>
  //   //   </div>
  //   // );
  // }
  public render(): React.ReactElement<IPeopleCarouselProps> {
    const responsive = {
      superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 5
      },
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 5
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 3
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 2
      }
    };
    return (
      <div className={ styles.peopleCarousel }>
        <div><p className="listHeaderTitle">Common Services Organization</p>
        <a className="listHeaderAll" href="https://cummins365.sharepoint.com/sites/CS45455_dev/SitePages/Common_Services_Org.aspx"><button className="listHeaderAllBtn">View All</button></a>
        </div>
        <div></div>
        <Carousel responsive={responsive} itemClass="carousel-item-padding-10-px"> 
        {this.state.items.map((emp, indx) => {
                            return (
                                  <div className="messagecardWrap">
                                    <div className="messagecard">
                                    <div className="divImageContainer"><img src={emp.DeptImage} alt="director" className="messageHeaderImg"></img></div>
                                      <img src={emp.Email} alt="profileImage" className="messagecardImage"></img>
                                      <h3 className="messagecardName">{emp.DisplayName}</h3>
                                      <p className="messagecardQuote">{emp.Title}</p>
                                      <div className="messagecardCompanyInfo">
                                        <p>Common Services</p>
                                        </div>
                                    </div>
                                  </div>
                              );
                            })}
        </Carousel>
        </div>
    );
  }
}
