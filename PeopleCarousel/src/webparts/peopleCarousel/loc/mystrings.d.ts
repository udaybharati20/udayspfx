declare interface IPeopleCarouselWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  PeoplePickerFieldLabel:string;
}

declare module 'PeopleCarouselWebPartStrings' {
  const strings: IPeopleCarouselWebPartStrings;
  export = strings;
}
