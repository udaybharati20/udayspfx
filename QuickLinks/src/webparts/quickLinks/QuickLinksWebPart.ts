import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  PropertyPaneLink
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './QuickLinksWebPart.module.scss';
import * as strings from 'QuickLinksWebPartStrings';
import {
  SPHttpClient,
  SPHttpClientResponse
} from '@microsoft/sp-http';
export interface IQuickLinksWebPartProps {
  description: string;
  ListURL:string;
}
export interface ISPLists {
  value: ISPList[];
}
export interface ISPList {
Title: string;
Link: object;
}
export default class QuickLinksWebPart extends BaseClientSideWebPart<IQuickLinksWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.quickLinks }">
        <div class="${ styles.container }">
          <div class="${ styles.row }">
          <h3><a href="https://cummins365.sharepoint.com/sites/CS45455_dev/Lists/Quick%20Links/AllItems.aspx">Quick Links</a></h3>
          <div id="spListContainer" class="${styles.QuickLinksCSS}">

          </div>
          </div>
        </div>
      </div>`;
      this._renderListAsync();
  }
  // public render(): void {
  //   this.domElement.innerHTML = `
  //     <div class="${ styles.quickLinks }">
  //       <div class="${ styles.container }">
  //         <div class="${ styles.row }">
  //         <h3><a href="https://cummins365.sharepoint.com/sites/CS45455_dev/">Quick Links</a></h3>
  //         <div class="${styles.QuickLinksCSS}">
  //           <ul>
  //             <li><a href="https://cummins365.sharepoint.com/">IT Operating Model - ITOM</a></li>
  //             <li><a href="https://cummins365.sharepoint.com/">Agile Journey</a></li>
  //             <li><a href="https://cummins365.sharepoint.com/">Project Portfolio Management - PPM Tool</a></li>
  //             <li><a href="https://cummins365.sharepoint.com/">Knowledge Base</a></li>
  //             <li><a href="https://cummins365.sharepoint.com/">IT Order Board</a></li>
  //             <li><a href="https://cummins365.sharepoint.com/">Enterprise IT Planning</a></li>
  //           </ul>
  //         </div>
  //         </div>
  //       </div>
  //     </div>`;
  // }
  private _renderListAsync(): void{
    this.getListData()
        .then((response) => {
          this._renderList(response.value);
        });
  }
  private getListData():Promise<ISPLists>
  {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl+"/_api/web/lists/GetByTitle('Quick%20Links')/Items?$top=6",SPHttpClient.configurations.v1)
    .then((response:SPHttpClientResponse)=>{
      return response.json();
    });
  }
  private _renderList(items: ISPList[]): void 
  {
   let html: string = '<ul>';
   items.forEach((item: ISPList) => {
    let linkvalue: object = item.Link;
    let linkURL=linkvalue["Url"];
     html += `
     <li><a href="${linkURL}">${item.Title}</a></li>
         `;
   });
 html += '</ul>';
   const listContainer: Element = this.domElement.querySelector('#spListContainer');
   listContainer.innerHTML = html;
 }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),
                PropertyPaneLink('ListURL',{
                  text:'Click here to update the content',href:'https://cummins365.sharepoint.com/sites/CS45455_dev/Lists/Quick%20Links/AllItems.aspx',target:'_blank'
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
