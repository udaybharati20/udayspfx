import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './PopularDocsWebPart.module.scss';
import * as strings from 'PopularDocsWebPartStrings';
import {
  SPHttpClient,
  SPHttpClientResponse
} from '@microsoft/sp-http';
import { startsWith } from 'lodash';
export interface IPopularDocsWebPartProps {
  description: string;
}
export interface ISPLists {
  value: ISPList[];
}
export interface ISPList {
Title: string;
docURL: string;
}
export default class PopularDocsWebPart extends BaseClientSideWebPart<IPopularDocsWebPartProps> {

  // public render(): void {
  //   this.domElement.innerHTML = `
  //     <div class="${ styles.popularDocs }">
  //       <div class="${ styles.container }">
  //         <div class="${ styles.row }">
  //           <div class="${ styles.column }">
  //             <span class="${ styles.title }">Welcome to SharePoint!</span>
  //             <p class="${ styles.subTitle }">Customize SharePoint experiences using Web Parts.</p>
  //             <p class="${ styles.description }">${escape(this.properties.description)}</p>
  //             <a href="https://aka.ms/spfx" class="${ styles.button }">
  //               <span class="${ styles.label }">Learn more</span>
  //             </a>
  //           </div>
  //         </div>
  //       </div>
  //     </div>`;
  // }
  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.popularDocs }">
        <h3>Trending Documents</h3>
        <div class="tab-content">
          <div class="tab-pane fade in active" id="tab1">
            <div class="${ styles.row}">
              <div id="spListContainer" class="${ styles['col-md-6']}">

              </div><!-- / .col-md-6 -->
            </div><!-- / row -->
            <div class="text-center" style="margin-top:10px;font-weight:600;">
              <a class="${ styles.btn } btn-default" href="https://cummins365.sharepoint.com/sites/CS45455_dev/Shared%20Documents/Forms/AllItems.aspx">View All Documents --></a>
            </div>
          </div>
        </div>
      </div>
      `;
      this._renderListAsync();
  }
  private _renderListAsync(): void{
    this.getSearchResults()
    .then((result : any[]) => {
      console.log("inside then"); 
        this._renderList(result);
    });
  }
  public getSearchResults(): Promise<ISPList[]> {  
          
    return new Promise<ISPList[]>((resolve, reject) => {  
        // Do an Ajax call to receive the search results  
        this.getListData().then((res: any) => {  
            let searchResp: ISPList[] = [];  
  
            // Check if there was an error  
            if (typeof res["odata.error"] !== "undefined") {  
                if (typeof res["odata.error"]["message"] !== "undefined") {  
                    Promise.reject(res["odata.error"]["message"].value);  
                    return;  
                }  
            }  
  
            if (!this._isNull(res)) {  
                const fields: string = "Title,ServerRedirectedURL,FileExtension,ViewsLifeTime,Author,ServerRedirectedEmbedURL";  
  
                // Retrieve all the table rows  
                if (typeof res.PrimaryQueryResult.RelevantResults.Table !== 'undefined') {  
                    if (typeof res.PrimaryQueryResult.RelevantResults.Table.Rows !== 'undefined') {  
                        searchResp = this._setSearchResults(res.PrimaryQueryResult.RelevantResults.Table.Rows, fields);  
                    }  
                }  
            }  
  
            // Return the retrieved result set  
            resolve(searchResp);  
        });  
    });  
  } 
  private _setSearchResults(crntResults: any[], fields: string): any[] {  
    console.log("_setSearchResults");
    const temp: any[] = [];  
  
    if (crntResults.length > 0) {  
        const flds: string[] = fields.toLowerCase().split(',');  
  
        crntResults.forEach((result) => {  
            // Create a temp value  
            var val: Object = {};  
  
            result.Cells.forEach((cell: any) => {  
                if (flds.indexOf(cell.Key.toLowerCase()) !== -1) {  
                    // Add key and value to temp value  
                    val[cell.Key] = cell.Value;  
                }  
            });  
  
            // Push this to the temp array  
            temp.push(val);  
            console.log("val"+val);
        });  
    }  
  
    return temp;  
  }
  private getListData():Promise<ISPLists>
  {
    let query = this.context.pageContext.web.absoluteUrl+"/_api/search/query?querytext='Path:https://cummins365.sharepoint.com/sites/CS45455_dev+IsDocument:True+FileExtension:doc*+FileExtension:pdf+FileExtension:x*'&selectproperties='Title,ServerRedirectedURL,FileExtension,ViewsLifeTime,Author,ServerRedirectedEmbedURL'&sortlist='ViewsLifeTime:descending'&rowlimit=3&refinmentfilters='ViewsLifeTime:not(null)'";
    console.log("query"+query);
    return this.context.spHttpClient.get(query,SPHttpClient.configurations.v1, {  
      headers: {  
          'odata-version': '3.0'  
      }  
  }).then((response:SPHttpClientResponse)=>{
      console.log("B"+response.status);
      return response.json();
    }).catch(error => {  
      return Promise.reject(JSON.stringify(error));  
  }); 
  }
  private _renderList(items: any[]): void 
  {
   let html: string = '';
   items.forEach((item: ISPList) => {
    let docType= item["FileExtension"];
    let docIcon ="";
    let docTitle = item["Title"];
    let docURL=item["ServerRedirectedURL"];
    let docView=item["ViewsLifeTime"]==null?0:item["ViewsLifeTime"];
    let docAuthor = item["Author"];
    if(startsWith(docType,"doc"))
    {
      docIcon = "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/TrendingDocs/word.png";
    }
    else if(startsWith(docType,"xl"))
    {
      docIcon = "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/TrendingDocs/excel.png";
    }
    else if(startsWith(docType,"pdf"))
    {
      docIcon = "https://cummins365.sharepoint.com/sites/CS45455_dev/Code/TrendingDocs/pdf.png";
      docURL=item["ServerRedirectedEmbedURL"];
    }
   
     html += `
     <div class="${ styles.media}">
               <div class="${ styles.docIcondiv}"><img class="${ styles.docIcon}" src="${docIcon}" alt="Event logo" /></div>
                 <div class="${ styles['media-body'] }">
                   <h4 class="${ styles['media-heading'] }">
                     <a href="${docURL}">${docTitle}</a>
                   </h4>
                   <div class="${ styles['meta-data'] }">
                     <span class="${ styles.creator }">- ${docAuthor}</span> <span class="${ styles.docViews }">${docView} views</span>
                   </div>
                 </div><!-- / media-body -->
                
               </div><!-- / media -->
               <hr>
         `;

   });
 //html += '</div>'
   const listContainer: Element = this.domElement.querySelector('#spListContainer');
   listContainer.innerHTML = html;
 }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }
  private _isNull(value: any): boolean {  
    return value === null || typeof value === "undefined";  
}  
  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
