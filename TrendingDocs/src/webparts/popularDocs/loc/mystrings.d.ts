declare interface IPopularDocsWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'PopularDocsWebPartStrings' {
  const strings: IPopularDocsWebPartStrings;
  export = strings;
}
