import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  PropertyPaneLink
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';
import styles from './UpcomingEventsWebPart.module.scss';
import * as strings from 'UpcomingEventsWebPartStrings';
import {
  SPHttpClient,
  SPHttpClientResponse
} from '@microsoft/sp-http';

//const logo :any = require('./assets/EventHeader.png');
export interface IUpcomingEventsWebPartProps {
  description: string;
  ConfigListLink:string;
}
export interface ISPLists {
  value: ISPList[];
}
export interface ISPList {
Title: string;
Details: string;
EventDate :string;
EndDate:string;
}
export const getDay = (date): string => {
  var date1 = new Date(date);
  var day = date1.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  return day;
};
export const getMonth = (date): string => {
  var date1 = new Date(date);
  var month = date1.getMonth();
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "July", "Aug", "Sep", "Oct", "Nov", "Dec"
];
  var mName = monthNames[month];
  return mName;
};
export const getTime = (date): string => {
  var date1 = new Date(date).toLocaleString('en-US', { timeZone: 'America/New_York' });
  var time = date1.split(' ')[1];
  return time;
};
export const FormatDate = (date): string => {
  var date1 = new Date(date);
  var year = date1.getFullYear();
  var month = (1 + date1.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;
  var day = date1.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  return month + '/' + day + '/' + year;
};
export default class UpcomingEventsWebPart extends BaseClientSideWebPart<IUpcomingEventsWebPartProps> {

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.upcomingEvents }">
      <div class="container">  
      <div>
        <img class="${styles.imgEvent}" src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Events/Event.png" alt="Event logo" width="100%"/>
        <div class="${ styles.eventHeaderText}"><a href="https://cummins365.sharepoint.com/sites/CS45455_dev/Lists/Events/AllItems.aspx" target="_blank">Latest Events</a></div>
      </div>
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
            <div class="${ styles.row}">
              <div id="spListContainer" class="${ styles['col-md-6']}">
                
              </div><!-- / .col-md-6 -->
            </div><!-- / row -->
          <div class="${ styles.divLine}"></div>
          <div class="text-center">
            <a class="${ styles.btn } btn-default" href="https://cummins365.sharepoint.com/sites/CS45455_dev/Lists/Events/AllItems.aspx">View All Events --></a>
          </div>
        </div>
        </div>
      </div>
    </div>
      </div>`;
      this._renderListAsync();
  }
  // public render(): void {
  //   this.domElement.innerHTML = `
  //     <div class="${ styles.upcomingEvents }">
  //     <div class="container">  
  //     <div>
  //       <img src="https://cummins365.sharepoint.com/sites/CS45455_dev/Code/Events/Event.png" alt="Event logo" width="100%" style="
  //       height: 140px;" />
  //       <div class="${ styles.eventHeaderText}">Latest Events</div>
  //     </div>
  //     <div class="tab-content">
  //       <div class="tab-pane fade in active" id="tab1">
  //         <div class="${ styles.row}">
  //           <div class="${ styles['col-md-6']}">
  //             <div class="${ styles.media}">
  //               <a class="${ styles['pull-left'] }" href="#"><span class="${ styles.dateEl }"><em>30</em>Sep</span></a>
  //               <div class="${ styles['media-body'] }">
  //                 <h4 class="${ styles['media-heading'] }">
  //                   <a href="https://cummins365.sharepoint.com/sites/CS45455_dev/Lists/Events/MyItems.aspx">Weekly Sales Meeting</a>
  //                 </h4>
  //                 <div class="${ styles['meta-data'] }">
  //                   <span class="${ styles.longDate }">Sep 30, 2021</span> <span class="${ styles.timeEl }">12:00pm - 02:00pm</span>
  //                 </div>
  //                 <p>
  //                   Scheduling this meeting to discuss sales report.
  //                 </p>
  //               </div><!-- / media-body -->
               
  //             </div><!-- / media -->
  //             <div class="${ styles.media}">
  //               <a class="${ styles['pull-left'] }" href="#"><span class="${ styles.dateEl }"><em>10</em>Oct</span></a>
  //               <div class="${ styles['media-body'] }">
  //                 <h4 class="${ styles['media-heading'] }">
  //                   <a href="#">Annual General Meeting of Members</a>
  //                 </h4>
  //                 <div class="${ styles['meta-data'] }">
  //                   <span class="${ styles.longDate }">Oct 10, 2021</span> <span class="${ styles.timeEl }">10:00am - 11:00am</span>
  //                 </div>
  //                 <p>
  //                   Discuss the plan for next year.
  //                 </p>
  //               </div><!-- / media-body -->
               
  //             </div><!-- / media -->
        
  //             <div class="${ styles.media}">
  //               <a class="${ styles['pull-left'] }" href="#"><span class="${ styles.dateEl }"><em>22</em>Oct</span></a>
  //               <div class="${ styles['media-body'] }">
  //                 <h4 class="${ styles['media-heading'] }">
  //                   <a href="#">Agile Training Session</a>
  //                 </h4>
  //                 <div class="${ styles['meta-data'] }">
  //                   <span class="${ styles.longDate }">Oct 22, 2021</span> <span class="${ styles.timeEl }">03:00pm - 04:00pm</span>
  //                 </div>
  //                 <p>
  //                   See the latest inspirations in the local tech scene.
  //                 </p>
  //               </div><!-- / media-body -->
               
  //             </div><!-- / media -->
  //           </div><!-- / .col-md-6 -->
  //         </div><!-- / row -->
  //         <div class="${ styles.divLine}"></div>
  //         <div class="text-center">
  //           <a class="${ styles.btn } btn-default" href="https://cummins365.sharepoint.com/sites/CS45455_dev/Lists/Events/AllItems.aspx">View All Events --></a>
  //         </div>
  //       </div>
  //       </div>
  //     </div>
  //   </div>
  //     </div>`;
  // }
  private _renderListAsync(): void{
    this.getListData()
        .then((response) => {
          this._renderList(response.value);
        });
  }
  private getListData():Promise<ISPLists>
  {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl+"/_api/web/lists/GetByTitle('Events')/Items?Select=ID,Title,Details,EventDate,EndDate&$top=3&$orderby=EventDate",SPHttpClient.configurations.v1)
    .then((response:SPHttpClientResponse)=>{
      return response.json();
    });
  }
  private _renderList(items: ISPList[]): void 
  {
   let html: string = '';
   items.forEach((item: ISPList) => {
     var d = getDay(item.EventDate);
     var m = getMonth(item.EventDate);
     var date = FormatDate(item.EventDate);
     var startTime = getTime(item.EventDate);
     var sTime= startTime.substr(0,startTime.length-3);
     var endTime = getTime(item.EndDate);
     var eTime = endTime.substr(0,endTime.length-3);
     let description = item.Details===null?"":item.Details.length>80?item.Details.substr(0,80):item.Details;
     let eventUrl = "https://cummins365.sharepoint.com/sites/CS45455_dev/Lists/Events/DispForm.aspx?ID="+item["Id"];
     html += `
     <div class="${ styles.media}">
     <a class="${ styles['pull-left'] }" href="#"><span class="${ styles.dateEl }"><em>${d}</em>${m}</span></a>
     <div class="${ styles['media-body'] }">
       <h4 class="${ styles['media-heading'] }">
         <a href="${eventUrl}">${item.Title}</a>
       </h4>
       <div class="${ styles['meta-data'] }">
         <span class="${ styles.longDate }">${date}</span> <span class="${ styles.timeEl }">${sTime} - ${eTime}</span>
       </div>
       <p>
        ${description}
       </p>
     </div><!-- / media-body -->
   </div><!-- / media -->
         `;
   });
 //html += '</div>'
   const listContainer: Element = this.domElement.querySelector('#spListContainer');
   listContainer.innerHTML = html;
 }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),
                PropertyPaneLink('ConfigListLink', {
                  text: "Click here to configure events",
                  href:"https://cummins365.sharepoint.com/sites/CS45455_dev/Lists/Events/AllItems.aspx",
                  target:"_blank"
                })
              ]
            }
          ]
        }
      ]
    };
  }
}

